package com.pages;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.Base.BaseTest;

public class Homepage extends BaseTest{
	
	public Homepage() throws IOException {
		super();
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//a[@href='/contacts']")
	WebElement contact;
	
	
	public void contacts() throws InterruptedException {
		contact.click();
		Thread.sleep(6000);
	}

}
