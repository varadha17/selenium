package com.stepDefinition;

import java.io.IOException;

import com.Base.BaseTest;
import com.pages.Homepage;
import com.pages.Loginpage;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;

public class HomepageTest extends BaseTest {
	
	public HomepageTest() throws IOException {
		super();
	}

	public Loginpage login;
	public Homepage home;
	
	@Before("@hometest")
	public void setup() throws InterruptedException, IOException {
		startBrowser();
		
		login = new Loginpage();
		
		home = login.validLogin();
	}
	
	@Given("^click on contacts icon$")
	public void click_on_contacts_icon() throws Throwable {
	  home.contacts();
	}
	
	@After("@hometest") 
	public void tearDown() {
		driver.quit();
	}
	
	
}
