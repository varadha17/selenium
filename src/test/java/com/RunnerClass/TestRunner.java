package com.RunnerClass;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;



    //-------------------------------------------------------//
//	  | ANDing, ORing tags and Ignoring tests				|
//	  | 													|
//	  |	OR  - {"@tag1 , @tag2"}								|
//    |	AND - {"@tag1","@tag2"}								|
//	  | Ignore cucumber test - {"~@tag1","~@tag2"}			|
//	  |														|
    //-------------------------------------------------------//

@RunWith(Cucumber.class)
@CucumberOptions(
		
		features= "src/main/java/com/feature/home.feature",
		glue= "src/test/java/com/com.stepDefinition", // path of step definition files
		//format={"pretty","html:test-output", "junit:junit_xml/cucumber.xml"}, // to generate different types of reports
		tags={"@home"}, // It will execute the tagged scenarios mentioned inside tags
		monochrome=true, // It will display the console output in proper readable format
		strict = false, // It will check if any step is not defined in step definition file
		dryRun = false // It will check the mapping between feature file and step definition file
		
		)

//GitLab:
// Step Def path: src/test/java/com/com.stepDefinition
//features path: src/main/java/com/feature/home.feature

//C:\\Users\\Abinash\\eclipse-workspace\\CucumberTest\\src\\main\\java\\com\\feature\\


public class TestRunner {

}
